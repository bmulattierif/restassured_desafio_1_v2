package core;

import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.enableLoggingOfRequestAndResponseIfValidationFails;
import static io.restassured.RestAssured.port;

import org.junit.BeforeClass;

import io.restassured.http.ContentType;

public class BaseTest implements Constantes {

	@BeforeClass
	public static void setup() {
		baseURI = "http://localhost/api/v1";
		basePath = "";
		port = 8888;
		@SuppressWarnings("unused")
		ContentType APP_CONTENT_TYPE = ContentType.JSON;
		
		enableLoggingOfRequestAndResponseIfValidationFails(); //Habilita el log solamente si tenemos fallo en el test
	}	
}
	

