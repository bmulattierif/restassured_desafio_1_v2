package test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.junit.Assert.*;

import org.junit.Test;

import core.BaseTest;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.Random;

public class SicrediTest extends BaseTest {

	Response response;
	RequestSpecification requestSpecification;
	String CPFConsultado;
	String CPFGenerado;
	String[] CPFConRestriccion = {"97093236014","60094146012","84809766080","62648716050","26276298085","01317496094",
								"55856777050","19626829001","2409492008","58063164083"};
	
	
	@Test
	public void queroConsultarUmCPF() {
		baseURI = APP_BASE_URL;
		port = APP_PORT;
		basePath = "restricoes";
		
		requestSpecification =  given();

		Random randomCPF = new Random();
		CPFConsultado = CPFConRestriccion[randomCPF.nextInt(CPFConRestriccion.length)];

		response = requestSpecification.get("/" + CPFConsultado).prettyPeek();

		assertThat(CPFConRestriccion, hasItemInArray(CPFConsultado));
		assertEquals(200, response.statusCode());
		assertEquals("O CPF " + CPFConsultado + " tem problema", response.body().path("mensagem"));
	}

	
	@Test
	public void queroGerarUmCPF() {
		baseURI = APP_BASE_URL;
		port = APP_PORT;
		basePath = "restricoes";
		requestSpecification = given();
		GeraCPF geraCpf = new GeraCPF();
		CPFGenerado = geraCpf.cpf(false);
		
		response= requestSpecification.get("/" + CPFGenerado).prettyPeek();
		
		assertEquals(204, response.statusCode());
		
	}
	
	@Test
	public void SimulacaoComSucesso() {
		GeraCPF geraCpf = new GeraCPF();
		CPFGenerado = geraCpf.cpf(false);
		SimulacionMetodos simulacion = new SimulacionMetodos();
		String body = SimulacionMetodos.configurarSimulacao(CPFGenerado,"Perico Perez", "provaTeste@provaTeste.com", 200.0f, 2, false);
		requestSpecification =  given().body(body).contentType(APP_CONTENT_TYPE);
					
	}
}
